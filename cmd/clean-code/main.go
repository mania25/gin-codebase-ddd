package main

import (
	"flag"
	"fmt"
	"log"
	"gitlab.com/mania25/gin-codebase-ddd/pkg/configs"
	httpHandler "gitlab.com/mania25/gin-codebase-ddd/pkg/deliveries/http"
	repositories "gitlab.com/mania25/gin-codebase-ddd/pkg/repositories/postgresql"
	usecase "gitlab.com/mania25/gin-codebase-ddd/pkg/usecases"
	"time"

	"github.com/gin-gonic/gin"

	"github.com/spf13/viper"
	logging "gopkg.in/tokopedia/logging.v1"

	"github.com/spf13/pflag"
)

var fDebug bool

func initializeDefaultFlag() {
	pflag.BoolVarP(&fDebug, "debug", "D", false, "activate debug mode")
}

func initLogger() {
	logging.LogInit()
	logging.SetDebug(fDebug)
	log.SetFlags(log.LstdFlags | log.Llongfile)
}

func main() {
	timeStart := time.Now()

	initializeDefaultFlag()
	configs.InitFlags()

	pflag.CommandLine.AddGoFlagSet(flag.CommandLine)
	pflag.Parse()
	viper.BindPFlags(pflag.CommandLine)

	initLogger()

	cfg, err := configs.NewConfig()

	if err != nil {
		log.Println(err)
		return
	}

	engine := gin.Default()

	db, errConn := repositories.NewPostgreSQLConnection(cfg.Postgre.ArticleDB.Master, cfg.Postgre.ArticleDB.Slave, 0)
	if errConn != nil {
		log.Println("[main] Error create connection to postgresql: ", errConn)
		return
	}

	log.Println("PostgreSQL connection has been established.")

	authorRepo := repositories.NewAuthorRepository(db)
	articleRepo := repositories.NewArticleRepository(db)

	timeoutContext := time.Duration(2000) * time.Second
	articleUsecases := usecase.NewArticleUsecase(articleRepo, authorRepo, timeoutContext)
	httpHandler.NewArticleHttpHandler(engine, articleUsecases)

	fmt.Println()
	log.Println("The service is up in", time.Since(timeStart).Seconds()*1000, "ms")
	fmt.Println()

	engine.Run(fmt.Sprintf(":%d", cfg.AppConfig.Port))
}
