package cleancode

import (
	"context"

	"gitlab.com/mania25/gin-codebase-ddd/pkg/models"
)

type ArticleRepo interface {
	Fetch(ctx context.Context, cursor string, num int64) (res []*models.Article, nextCursor string, err error)
	GetByID(ctx context.Context, id int64) (*models.Article, error)
	GetByTitle(ctx context.Context, title string) (*models.Article, error)
	Update(ctx context.Context, ar *models.Article) error
	Store(ctx context.Context, a *models.Article) error
	Delete(ctx context.Context, id int64) error
}

// Repository represent the author's repository contract
type AuthorRepo interface {
	GetByID(ctx context.Context, id int64) (*models.Author, error)
}
