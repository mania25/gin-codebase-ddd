package http

import (
	"context"
	"log"
	"net/http"
	"strconv"

	cleancode "gitlab.com/mania25/gin-codebase-ddd/pkg"
	"gitlab.com/mania25/gin-codebase-ddd/pkg/models"

	"github.com/gin-gonic/gin"
	validator "gopkg.in/go-playground/validator.v9"
)

type ResponseError struct {
	Message string `json:"message"`
}

type HttpArticleHandler struct {
	ArticleUsecases cleancode.ArticleUsecases
}

func NewArticleHttpHandler(gin *gin.Engine, articleUsecases cleancode.ArticleUsecases) {
	handler := &HttpArticleHandler{
		ArticleUsecases: articleUsecases,
	}

	gin.GET("/", handler.DefaultRoute)
	gin.GET("/articles", handler.FetchArticle)
	gin.POST("/articles", handler.Store)
	gin.GET("/articles/:id", handler.GetByID)
	gin.DELETE("/articles/:id", handler.Delete)
}

func (a *HttpArticleHandler) DefaultRoute(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{"version": "0.0.1"})
}

func (a *HttpArticleHandler) FetchArticle(c *gin.Context) {
	numS := c.Query("num")
	num, _ := strconv.Atoi(numS)
	cursor := c.Query("cursor")

	ctx := context.Background()

	listAr, nextCursor, err := a.ArticleUsecases.Fetch(ctx, cursor, int64(num))

	if err != nil {
		c.JSON(getStatusCode(err), ResponseError{Message: err.Error()})
		return
	}

	c.Header(`X-Cursor`, nextCursor)
	c.JSON(http.StatusOK, listAr)
}

func (a *HttpArticleHandler) GetByID(c *gin.Context) {
	idP, err := strconv.Atoi(c.Param("id"))
	id := int64(idP)

	ctx := context.Background()

	art, err := a.ArticleUsecases.GetByID(ctx, id)

	if err != nil {
		c.JSON(getStatusCode(err), ResponseError{Message: err.Error()})
		return
	}

	c.JSON(http.StatusOK, art)
}

func isRequestValid(m *models.Article) (bool, error) {
	validate := validator.New()

	err := validate.Struct(m)
	if err != nil {
		return false, err
	}
	return true, nil
}

func (a *HttpArticleHandler) Store(c *gin.Context) {
	var article models.Article
	err := c.Bind(&article)
	if err != nil {
		c.JSON(http.StatusUnprocessableEntity, err.Error())
		return
	}

	if ok, err := isRequestValid(&article); !ok {
		c.JSON(http.StatusBadRequest, err.Error())
		return
	}

	ctx := context.Background()

	err = a.ArticleUsecases.Store(ctx, &article)

	if err != nil {
		c.JSON(getStatusCode(err), ResponseError{Message: err.Error()})
		return
	}
	c.JSON(http.StatusCreated, article)
}

func (a *HttpArticleHandler) Delete(c *gin.Context) {
	idP, err := strconv.Atoi(c.Param("id"))
	id := int64(idP)

	ctx := context.Background()

	err = a.ArticleUsecases.Delete(ctx, id)

	if err != nil {
		c.JSON(getStatusCode(err), ResponseError{Message: err.Error()})
		return
	}

	c.Status(http.StatusNoContent)
}

func getStatusCode(err error) int {

	if err == nil {
		return http.StatusOK
	}
	log.Println(err)
	switch err {
	case models.ErrInternalServerError:
		return http.StatusInternalServerError
	case models.ErrNotFound:
		return http.StatusNotFound
	case models.ErrConflict:
		return http.StatusConflict
	default:
		return http.StatusInternalServerError
	}
}
