package repositories

import (
	"context"
	"database/sql"
	"encoding/base64"
	"fmt"
	"log"
	"time"

	cleancode "gitlab.com/mania25/gin-codebase-ddd/pkg"
	"gitlab.com/mania25/gin-codebase-ddd/pkg/models"
)

const (
	timeFormat = "2006-01-02T15:04:05.999Z07:00" // reduce precision from RFC3339Nano as date format
)

type articleRepository struct {
	Conn *sql.DB
}

func NewArticleRepository(Conn *sql.DB) cleancode.ArticleRepo {
	return &articleRepository{Conn}
}

func (article *articleRepository) fetch(ctx context.Context, query string, args ...interface{}) ([]*models.Article, error) {
	rows, err := article.Conn.QueryContext(ctx, query, args...)

	if err != nil {
		return nil, fmt.Errorf("[repositories][fetch] Failed to initialize query context: %v", err)
	}

	defer rows.Close()

	result := make([]*models.Article, 0)
	for rows.Next() {
		t := new(models.Article)
		authorID := int64(0)
		err = rows.Scan(
			&t.ID,
			&t.Title,
			&t.Content,
			&authorID,
			&t.UpdatedAt,
			&t.CreatedAt,
		)

		if err != nil {
			return nil, fmt.Errorf("[repositories][fetch] Failed to scan rows data: %v", err)
		}

		t.Author = models.Author{
			ID: authorID,
		}

		result = append(result, t)
	}

	return result, nil
}

func (article *articleRepository) Fetch(ctx context.Context, cursor string, num int64) ([]*models.Article, string, error) {

	query := `SELECT id,title,content, author_id, updated_at, created_at
  						FROM article WHERE created_at > $1 ORDER BY created_at LIMIT $2`

	decodedCursor, err := DecodeCursor(cursor)
	if err != nil && cursor != "" {
		return nil, "", models.ErrBadParamInput
	}

	res, err := article.fetch(ctx, query, decodedCursor, num)
	if err != nil {
		return nil, "", err
	}

	nextCursor := ""
	if len(res) == int(num) {
		nextCursor = EncodeCursor(res[len(res)-1].CreatedAt)
	}

	return res, nextCursor, err
}

func (article *articleRepository) GetByID(ctx context.Context, id int64) (*models.Article, error) {
	query := `SELECT id,title,content, author_id, updated_at, created_at
  						FROM article WHERE ID = $1`

	list, err := article.fetch(ctx, query, id)
	if err != nil {
		return nil, err
	}

	a := &models.Article{}
	if len(list) > 0 {
		a = list[0]
	} else {
		return nil, models.ErrNotFound
	}

	return a, nil
}

func (article *articleRepository) GetByTitle(ctx context.Context, title string) (*models.Article, error) {
	query := `SELECT id,title,content, author_id, updated_at, created_at
  						FROM article WHERE title = $1`

	list, err := article.fetch(ctx, query, title)
	if err != nil {
		return nil, err
	}

	a := &models.Article{}
	if len(list) > 0 {
		a = list[0]
	} else {
		return nil, models.ErrNotFound
	}
	return a, nil
}

func (article *articleRepository) Store(ctx context.Context, a *models.Article) error {

	query := `INSERT  article SET title=$1 , content=$2 , author_id=$3, updated_at=$4 , created_at=$5`
	stmt, err := article.Conn.PrepareContext(ctx, query)
	if err != nil {

		return err
	}

	log.Println("Created At: ", a.CreatedAt)
	res, err := stmt.ExecContext(ctx, a.Title, a.Content, a.Author.ID, a.UpdatedAt, a.CreatedAt)
	if err != nil {

		return err
	}
	lastId, err := res.LastInsertId()
	if err != nil {
		return err
	}
	a.ID = lastId
	return nil
}

func (article *articleRepository) Delete(ctx context.Context, id int64) error {
	query := "DELETE FROM article WHERE id = $1"

	stmt, err := article.Conn.PrepareContext(ctx, query)
	if err != nil {
		return err
	}
	res, err := stmt.ExecContext(ctx, id)
	if err != nil {

		return err
	}
	rowsAfected, err := res.RowsAffected()
	if err != nil {
		return err
	}
	if rowsAfected != 1 {
		err = fmt.Errorf("Weird  Behaviour. Total Affected: %d", rowsAfected)
		return err
	}

	return nil
}
func (article *articleRepository) Update(ctx context.Context, ar *models.Article) error {
	query := `UPDATE article set title=$1, content=$2, author_id=$3, updated_at=$4 WHERE ID = $5`

	stmt, err := article.Conn.PrepareContext(ctx, query)
	if err != nil {
		return nil
	}

	res, err := stmt.ExecContext(ctx, ar.Title, ar.Content, ar.Author.ID, ar.UpdatedAt, ar.ID)
	if err != nil {
		return err
	}
	affect, err := res.RowsAffected()
	if err != nil {
		return err
	}
	if affect != 1 {
		err = fmt.Errorf("Weird  Behaviour. Total Affected: %d", affect)

		return err
	}

	return nil
}

func DecodeCursor(encodedTime string) (time.Time, error) {
	byt, err := base64.StdEncoding.DecodeString(encodedTime)
	if err != nil {
		return time.Time{}, err
	}

	timeString := string(byt)
	t, err := time.Parse(timeFormat, timeString)

	return t, err
}

func EncodeCursor(t time.Time) string {
	timeString := t.Format(timeFormat)

	return base64.StdEncoding.EncodeToString([]byte(timeString))
}
