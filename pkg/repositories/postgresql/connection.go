package repositories

import (
	"database/sql"
	"fmt"

	_ "github.com/lib/pq"
)

func NewPostgreSQLConnection(master, slave string, maxConn int) (client *sql.DB, err error) {
	DSN := fmt.Sprintf("%s;%s", master, slave)
	if master == slave {
		DSN = master
	}

	client, err = sql.Open("postgres", DSN)

	if maxConn == 0 {
		client.SetMaxOpenConns(100)
	} else {
		client.SetMaxOpenConns(maxConn)
	}

	return
}
