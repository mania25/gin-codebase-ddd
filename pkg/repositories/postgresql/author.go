package repositories

import (
	"context"
	"database/sql"
	"log"

	cleancode "gitlab.com/mania25/gin-codebase-ddd/pkg"
	"gitlab.com/mania25/gin-codebase-ddd/pkg/models"
)

type authorRepo struct {
	DB *sql.DB
}

func NewAuthorRepository(db *sql.DB) cleancode.AuthorRepo {
	return &authorRepo{DB: db}
}

func (author *authorRepo) getOne(ctx context.Context, query string, args ...interface{}) (*models.Author, error) {

	stmt, err := author.DB.PrepareContext(ctx, query)
	if err != nil {
		log.Println(err)
		return nil, err
	}
	row := stmt.QueryRowContext(ctx, args...)
	a := &models.Author{}

	err = row.Scan(
		&a.ID,
		&a.Name,
		&a.CreatedAt,
		&a.UpdatedAt,
	)
	if err != nil {
		log.Println(err)
		return nil, err
	}

	return a, nil
}

func (author *authorRepo) GetByID(ctx context.Context, id int64) (*models.Author, error) {
	query := `SELECT id, name, created_at, updated_at FROM author WHERE id=$1`
	return author.getOne(ctx, query, id)
}
