package configs

import (
	"fmt"
	"log"
	"os"
	"time"

	"gitlab.com/mania25/gin-codebase-ddd/pkg/models"

	"github.com/fsnotify/fsnotify"
	"github.com/spf13/viper"
)

func NewConfig() (runtimeConf *models.RuntimeConfig, err error) {
	if viper.GetBool("enable-remote-config") {
		return initRemoteConfig(viper.GetString("remote-config-provider"), viper.GetString("remote-config-hostname"), viper.GetString("remote-config-target"), viper.GetString("remote-config-type"))
	}

	if os.Getenv("MODE") == "" {
		return nil, fmt.Errorf("[configs][NewConfig] Cannot read MODE env variable")
	}

	return initReadConfigFile(os.Getenv("MODE"), "toml", "../../config/")
}

func initRemoteConfig(remoteProvider, remoteHostname, target, fileType string) (runtimeConf *models.RuntimeConfig, err error) {
	runtimeViper := viper.New()

	runtimeViper.AddRemoteProvider(remoteProvider, remoteHostname, target)
	runtimeViper.SetConfigType(fileType)

	err = runtimeViper.ReadRemoteConfig()
	if err != nil {
		return nil, fmt.Errorf("[configs][InitRemoteConfig] Failed to read remote config: %v", err)
	}

	err = runtimeViper.Unmarshal(&runtimeConf)
	if err != nil {
		return nil, fmt.Errorf("[configs][InitRemoteConfig] Failed to unmarshal remote config: %v", err)
	}

	// open a goroutine to watch remote changes forever
	go func() {
		for {
			time.Sleep(time.Second * 1) // delay after each request

			// currently, only tested with etcd support
			err := runtimeViper.WatchRemoteConfig()
			if err != nil {
				log.Println("[configs][InitRemoteConfig] Unable to read remote config:", err)
				continue
			}

			// unmarshal new config into our runtime config struct. you can also use channel
			// to implement a signal to notify the system of the changes
			err = runtimeViper.Unmarshal(&runtimeConf)
			if err != nil {
				log.Println("[configs][InitRemoteConfig] Failed to unmarshal remote config:", err)
			}
		}
	}()

	return
}

func initReadConfigFile(fileName, fileType, pathFile string) (runtimeConf *models.RuntimeConfig, err error) {
	runtimeViper := viper.New()

	runtimeViper.SetConfigName(fileName)
	runtimeViper.SetConfigType(fileType)
	runtimeViper.AddConfigPath(pathFile)

	err = runtimeViper.ReadInConfig()
	if err != nil {
		return nil, fmt.Errorf("[configs][InitReadConfigFile] Unable to read config file: %v", err)
	}

	err = runtimeViper.Unmarshal(&runtimeConf)
	if err != nil {
		return nil, fmt.Errorf("[configs][InitReadConfigFile] Failed to unmarshal config: %v", err)
	}

	runtimeViper.WatchConfig()
	runtimeViper.OnConfigChange(func(e fsnotify.Event) {
		log.Println("[configs][InitReadConfigFile] Config changed", e.Name)

		err := runtimeViper.ReadInConfig()
		if err != nil { // Handle errors reading the config file
			log.Printf("Fatal error config file: %s \n", err)
		}

		// unmarshal config
		runtimeViper.Unmarshal(&runtimeConf)
	})

	return
}
