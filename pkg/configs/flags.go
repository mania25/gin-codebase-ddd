package configs

import "github.com/spf13/pflag"

func InitFlags() {
	pflag.Bool("enable-remote-config", false, "Enable Remote Config")

	pflag.String("remote-config-provider", "consul", "Set Remote Config Provider (etcd, consul *default, others)")
	pflag.String("remote-config-hostname", "localhost:8500", "Set Remote Config Hostname (default using consul localhost)")
	pflag.String("remote-config-target", "", "Set Remote Config Target Key")
	pflag.String("remote-config-type", "toml", "Set Remote Config File Type (default to TOML)")
}
