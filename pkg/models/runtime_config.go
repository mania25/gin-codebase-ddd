package models

type (
	RuntimeConfig struct {
		AppConfig     *appConfig     `toml:"AppConfig"`
		Postgre       *postgreConfig `toml:"Postgre"`
		Redis         *redisConfig   `toml:"Redis"`
		Elasticsearch *elasticConfig `toml:"Elasticsearch"`
	}

	appConfig struct {
		AppName string `toml:"appName"`
		Port    int    `toml:"port"`
	}

	postgreConfig struct {
		ArticleDB *postgreArticleConfig `toml:"ArticleDB"`
	}

	redisConfig struct {
		Article *redisArticleConfig `toml:"Article"`
	}

	elasticConfig struct {
		Article *elasticArticleConfig `toml:"Article"`
	}

	postgreArticleConfig struct {
		Enabled       bool   `toml:"enabled"`
		Master        string `toml:"master"`
		Slave         string `toml:"slave"`
		MaxConnection int    `toml:"maxConnection"`
	}

	redisArticleConfig struct {
		Enabled       bool   `toml:"enabled"`
		ConnectionURI string `toml:"connectionURI"`
		MaxActiveConn int    `toml:"maxActiveConn"`
	}

	elasticArticleConfig struct {
		Enabled bool   `toml:"enabled"`
		DSN     string `toml:"DSN"`
		Timeout string `toml:"Timeout"`
		Index   string `toml:"Index"`
	}
)
