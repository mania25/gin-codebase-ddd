BINARY=article
test: 
	go test -v -cover -covermode=atomic ./...

vendor:
	@dep ensure -v

article: vendor
	go build -tags=jsoniter -o ./${BINARY} gitlab.com/mania25/gin-codebase-ddd/cmd/clean-code

install: 
	go build -tags=jsoniter -o ./${BINARY} gitlab.com/mania25/gin-codebase-ddd/cmd/clean-code

unittest:
	go test -short $$(go list ./... | grep -v /vendor/)

clean:
	if [ -f ${BINARY} ] ; then rm ${BINARY} ; fi

.PHONY: clean install unittest build docker run stop vendor